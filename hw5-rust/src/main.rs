use aws_config::load_from_env;
use chrono::NaiveWeek;
use lambda_runtime::{service_fn, LambdaEvent, Error};
use serde_json::Value;
use aws_sdk_dynamodb::Client;
use aws_sdk_dynamodb::model::AttributeValue;
use std::collections::HashMap;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Product {
    pub item_id: String,
    pub name: String,
    pub quantity: i32,
    pub unit_price: f32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InventoryRequest {
    pub item_id: String,  // Unique identifier for the item
    pub change_in_quantity: i32, // The change in quantity (can be positive or negative)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(lambda_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

#[derive(Serialize)]
struct LambdaResponse {
    message: String,
}

async fn lambda_handler(event: LambdaEvent<Value>) -> Result<LambdaResponse, Error> {
    let request: InventoryRequest = serde_json::from_value(event.payload)?;
    let config = load_from_env().await;
    let client = Client::new(&config);

    let result = update_inventory(&client, &request).await?;

    Ok(LambdaResponse {
        message: result,
    })
}


// Function to add or subtract from an item's quantity in the inventory table

async fn update_inventory(client: &Client, request: &InventoryRequest) -> Result<String, Error> {
    let table_name = "inventory";

    let key = HashMap::from([
        ("item_id".to_string(), AttributeValue::S(request.item_id.clone())),
    ]);

    let current_item_result = client.get_item()
        .table_name(table_name)
        .set_key(Some(key))
        .send()
        .await?;

    let mut new_quantity = request.change_in_quantity;

    if let Some(item) = current_item_result.item {
        if let Some(quantity_attr) = item.get("quantity") {
            let quantity_str = quantity_attr.as_n().map(|s| s.to_string()).unwrap_or("0".to_string());
        }
    }

    let update_expression = "SET quantity = :new_quantity";
    let mut expression_attribute_values = HashMap::new();
    expression_attribute_values.insert(":new_quantity".to_string(), AttributeValue::N(new_quantity.to_string()));

    client.update_item()
        .table_name(table_name)
        .key("item_id", AttributeValue::S(request.item_id.clone()))
        .update_expression(update_expression)
        .set_expression_attribute_values(Some(expression_attribute_values))
        .send()
        .await
        .map_err(|_| Error::from("Could not update the item"))?;

    Ok(format!("Updated item_id {}: new quantity is {}", request.item_id, new_quantity))
}

# hw5

## Rust lambda function

### Lambda Handler

The entry point for the Lambda function is defined in the `main` function, which sets up `lambda_handler` as the function to be called upon invocation.

```rust
async fn main() -> Result<(), Error> {
    let func = service_fn(lambda_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}
```

### InventoryRequest

A request to this service should be a JSON object that conforms to the `InventoryRequest` struct:

```rust
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct InventoryRequest {
    pub item_id: String,  // Unique identifier for the item
    pub change_in_quantity: i32, // The change in quantity (can be positive or negative)
}
```
- Represents a request to change the quantity of an item.

### Lambda Handler
- Entry point for the Lambda function.
- Parses the event payload into an InventoryRequest.
- Calls update_inventory to adjust the quantity of the item.

### Update Inventory Function
- Retrieves the current quantity of the item from DynamoDB.
- Calculates the new quantity.
- Updates the item's quantity in the DynamoDB table.

## Building and Deploying the Lambda Function

### Building the Lambda Function
```bash
cargo lambda build --release
```

### Deploying the Lambda Function
```bash
cargo lambda deploy
```
- Add API Gateway trigger to the lambda function
- Grant the lambda function permission to access the DynamoDB table

## Building DynamoDB Table and Testing(screenshots)

1. Create a new table in DynamoDB
![create table](cmd_1.png)


2. Add items to the table
![add items](put_item1.png)
![p](put_item2.png)
![p](put_item3.png)
table:
![original database](original_db.png)

3. Test the lambda function
![test lambda](test.png)
```bash
aws lambda invoke --function-name hw5-rust --cli-binary-format raw-in-base64-out --payload "$(echo -n '{"item_id": "item2", "change_in_quantity": 100}')" response.json
```
result:
![updated database](modified_db.png)
We can see that the quantity of item2 has been updated from 200 to 100, which is the expected result.





